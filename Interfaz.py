from tkinter import *
import tkinter as tk
from PIL import ImageTk
from PIL import Image
from PIL import *
from tkinter import filedialog
import io
import requests
from io import BytesIO
import sys
import requests
import json
import pickle
import cognitive_face as CF
from PIL import Image, ImageDraw, ImageFont


subscription_key = None

SUBSCRIPTION_KEY = '2da64ff4594e4e05bfe4a77973079b9a'
BASE_URL = 'https://miscaras.cognitiveservices.azure.com/face/v1.0/'
CF.BaseUrl.set(BASE_URL)
CF.Key.set(SUBSCRIPTION_KEY)
personas = []
####Crear clase Persona y Estudiante
class Persona():
    """Clase Persona"""
    def __init__(self, save_iden, person_id, save_name, gender, age, picture):
        self.save_iden = save_iden
        self.person_id = person_id
        self.save_name = save_name 
        self.gender = gender
        self.age = age
        self.picture = picture


class Estudiante1(Persona):
    """Clase Estudiante"""
    def __init__(self, save_iden, person_id, save_name, gender, age, picture, save_p1,save_p2,save_p3):
        Persona.__init__(self, save_iden, person_id, save_name, gender, age, picture)
        self.save_p1=save_p1
        self.save_p2=save_p2
        self.save_p3=save_p3

class profesor1(Persona):
    """Clase profesor"""
    def __init__(self, save_iden, person_id, save_name, gender, age, picture,  save_p1,save_p2,save_p3):
        Persona.__init__(self, save_iden, person_id, save_name, gender, age, picture)
        self.save_p1=save_p1
        self.save_p2=save_p2
        self.save_p3=save_p3

class administrativo1(Persona):
    """Clase administrativo"""
    def __init__(self, save_iden, person_id, save_name, gender, age, picture,  save_p1,save_p2,save_p3):
        Persona.__init__(self, save_iden, person_id, save_name, gender, age, picture)
        self.save_p1=save_p1
        self.save_p2=save_p2
        self.save_p3=save_p3


class Ventana(tk.Frame):
    def __init__(self, master=None):
        super(Ventana,self).__init__(master=master)
        Frame.__init__(self,master)#parametros que usted quiere enviar a traves de la clase frame
        self.master= master
        self.inciar_menu()#mostrar menu
        self.label = tk.Label(self, text = "Microsoft azure")
        self.label.pack(padx = 20, pady= 20)

    
    def inciar_menu(self):
        self.master.title("GUI")
        self.pack(fill=BOTH, expand = 100)
        #crear instancia menu
        menu = Menu(self.master)

        self.master.config(menu=menu)
        
        #crear el objeto archivo
        archivo = Menu(menu)
        #Agregar comandos  la opcion del menu 
        #Opcion salir
        archivo.add_command(label="Salir", command=self.salir)
        menu.add_cascade(label="Archivo", menu=archivo)

        
        archivo.add_command(label="Crear grupo", command=self.g_ventana1)
        archivo.add_command(label="Crear persona", command=self.g_ventana2)
        archivo.add_command(label="Identificar persona", command=self.g_ventana3)
        

   
    def salir (self):
        exit()
    
    
    
    
    def g_ventana1(self):
        
        def create_group1():
            global save_name 
            global save_group
            name = entry_1.get()
            save_name =  name
            label_2["text"] = save_name

            id_group=entry_2.get()
            save_group = id_group
            label_4["text"] = save_group
            
            CF.person_group.create(save_group, save_name)
            print("Grupo creado")
            
        my_window = tk.Tk()
        label_1=tk.Label(my_window,text = "Digite el nombre de grupo")
        entry_1 = tk.Entry(my_window) #guarda valor de label_1
        button_1 = tk.Button(my_window,text = "Guardar", command=create_group1)
        label_2=tk.Label(my_window)


        label_3 = tk.Label(my_window, text = "Digite numero del grupo")
        entry_2 = tk.Entry(my_window) #guarda numero
        label_4=tk.Label(my_window)
        
        label_1.grid(row=0,column=0)
        entry_1.grid(row=0,column=6)

        label_3.grid(row=1,column= 0)
        entry_2.grid(row=1,column=6)

        button_1.grid(row=5,column=0)
        label_2.grid(row=5,column=1)
        label_4.grid(row=6,column=1)
    
    def g_ventana2 (self):
        
        def crear_persona():
           
            iden = entry_1.get()
            save_iden = iden
            label_2["text"] = save_iden

            name=entry_2.get()
            save_name = name
            label_4["text"] = save_name

            picture = entry_3.get()
            label_url["text"]=picture

            p1 = entry_4.get()
            save_p1 = p1
            label_p1 ["text"]=save_p1

            p2 = entry_5.get()
            save_p2 = p2
            label_p2 ["text"]=save_p2

            p3 = entry_6.get()
            save_p3 = p3
            label_p3 ["text"]=save_p3

            group_id=entry_7.get()
            label_group["text"]= group_id
            
            
            def emotions(picture):
                headers = {'Ocp-Apim-Subscription-Key': 'e70e11c9cb684f21b8b37313fd60e5bc'}
                image_path = picture
                #https://docs.microsoft.com/en-us/azure/cognitive-services/computer-vision/quickstarts/python-disk
                # Read the image into a byte array
                image_data = open(image_path, "rb").read()
                headers = {'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
                'Content-Type': 'application/octet-stream'}
                params = {
                    'returnFaceId': 'true',
                    'returnFaceLandmarks': 'false',
                    'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
                }
                response = requests.post(
                                        BASE_URL + "detect/", headers=headers, params=params, data=image_data)
                analysis = response.json()
                #print(analysis)
                return analysis

            def create_person(group_id, save_iden, save_name, picture,  save_p1, save_p2, save_p3):
                response = CF.person.create(group_id, save_name)
                person_id = response['personId']
                CF.person.add_face(picture, group_id, person_id)
                CF.person_group.train(group_id)
                response = CF.person_group.get_status(group_id)
                status = response['status']
                print(status)
                face_list = emotions(picture) #Muy importante la función emotions debe reportar la lista
                dic1 = face_list[0]
                faceAttributes = dic1['faceAttributes']
                gender = faceAttributes['gender']
                age = faceAttributes['age']
                 #Save in a file
                if group_id == "125":
                    with open("alumn.bin", "ab") as f:
                        e1 = Estudiante1(save_iden,person_id,save_name,gender,age,picture,save_p1,save_p2,save_p3)
                        #print(e1.save_iden,e1.person_id,e1.save_name,e1.gender,e1.age, e1.save_p1, e1.save_p1, e1.save_p1)
                        pickle.dump(e1, f, pickle.HIGHEST_PROTOCOL)
                elif group_id =="225":
                     with open("profezor.bin", "ab") as f:
                        p1 = profesor1(save_iden,person_id,save_name,gender,age,picture,save_p1,save_p2,save_p3)
                        #print(e1.save_iden,e1.person_id,e1.save_name,e1.gender,e1.age, e1.save_p1, e1.save_p1, e1.save_p1)
                        pickle.dump(p1, f, pickle.HIGHEST_PROTOCOL)
                elif group_id =="325":
                     with open("admini.bin", "ab") as f:
                        a1 = administrativo1(save_iden,person_id,save_name,gender,age,picture,save_p1,save_p2,save_p3)
                        #print(e1.save_iden,e1.person_id,e1.save_name,e1.gender,e1.age, e1.save_p1, e1.save_p1, e1.save_p1)
                        pickle.dump(a1, f, pickle.HIGHEST_PROTOCOL)
                        
                def read_file_estudiante():
                    f = open("alumn.bin", "rb")
                    f.seek(0)
                    flag = 0
                    while flag == 0:
                        try:
                            e = pickle.load(f)
                            print(e.save_iden, e.save_name, e.person_id,e.gender,e.age, e1.save_p1, e.save_p2, e.save_p3 )
                            
                        except:
                        
                            flag = 1      
                    f.close()
                def read_file_profesor():
                    f = open("profezor.bin", "rb")
                    f.seek(0)
                    flag = 0
                    while flag == 0:
                        try:
                            p = pickle.load(f)
                            print(p.save_iden, p.save_name, p.person_id,p.gender,p.age, p.save_p1, p.save_p2, p.save_p3 )
                            
                        except:
                        
                            flag = 1      
                    f.close()
                def read_file_admini():
                    f = open("admini.bin", "rb")
                    f.seek(0)
                    flag = 0
                    while flag == 0:
                        try:
                            a = pickle.load(f)
                            print(a.save_iden, a.save_name, a.person_id,a.gender,a.age, a.save_p1, a.save_p2, a.save_p3 )
                            
                        except:
                        
                            flag = 1      
                    f.close()
                if group_id == "125":
                    print("Archivo binario de estudiante")
                    read_file_estudiante()
                elif group_id == "225":
                    print("Archivo binario de profesor")
                    read_file_profesor()
                elif group_id =="325":
                    print("Archivo binario de administradores")
                    read_file_admini()
                lista1=[]
                def lista_estudiante (lista1):
                    f = open("alumn.bin", "rb")
                    f.seek(0)
                    flag = 0
                    
                    while flag == 0:
                        try:
                            e = pickle.load(f) 

                            lista1.append(e.save_iden)
                            lista1.append(e.save_name)
                            lista1.append(e.person_id)
                            lista1.append(e.gender)
                            lista1.append(e.age)
                            lista1.append(e.save_p1)
                            lista1.append(e.save_p2)
                            lista1.append(e.save_p3)
                            return lista1
                            
                            
                        except:
                            flag=1
                listap= []
                def lista_profesor(listap):
                    f = open("Profezor.bin", "rb")
                    f.seek(0)
                    flag = 0
                    while flag == 0:
                        try:
                            p = pickle.load(f)
                            listap.append(p.save_iden)
                            listap.append(p.save_name)
                            listap.append(p.person_id)
                            listap.append(p.gender)
                            listap.append(p.age)
                            listap.append(p.save_p1)
                            listap.append(p.save_p2)
                            listap.append(p.save_p3)
                            
                            return listap
                        except:
                            
                            
                            flag=1
                
                listaa=[]
                def lista_admin(listaa):
                    f = open("Admini.bin", "rb")
                    f.seek(0)
                    flag = 0
                    while flag == 0:
                        try:
                            a = pickle.load(f)
                            
                            listaa.append(a.save_iden)
                            listaa.append(a.save_name)
                            listaa.append(a.person_id)
                            listaa.append(a.gender)
                            listaa.append(a.age)
                            listaa.append(a.save_p1)
                            listaa.append(a.save_p2)
                            listaa.append(a.save_p3)
                            
                            return listaa
                
                            
                        except:
                            
                            flag = 1
                lista = []        
                def lista_f(lista):
                    lista.append(lista_admin(listaa))
                    lista.append(lista_estudiante(lista1))
                    lista.append(lista_profesor(listap))
                    print(lista)
                    return lista
                
                def ordenamiento (lista):
                    lista = lista_f(lista)
                    for i in range (1, len(lista)):
                        actual = lista[i]
                        indice = i
                        while indice > 0 and lista [indice-1] > actual:
                            lista[indice]=lista[indice-1]
                            indice = indice -1   
                        lista[indice] = actual
                    print(lista)
                
                def imp_lista_admi()  :
                    x = lista_admin(listaa)
                    
                    print(x)
                    x.reverse()
                    print("")
                    print (x)

                def imp_lista_est()  :
                    z = lista_estudiante(lista1)
                    
                    print(z)
                    z.reverse()
                    print("")
                    print (z)

                def imp_lista_prof()  :
                    x = lista_profesor(listap)
                    
                    print(x)
                    x.reverse()
                    print("")
                    print (x)
                
                if group_id =="125":
                    print("lista de estudiantes: ")
                    imp_lista_est()
                elif group_id =="225":
                    print("lista de profesores: ")
                    imp_lista_prof()
                elif group_id == "325":
                    print("lista de administradores ")
                    imp_lista_admi()
                
                print("La lista de objetos ordenada por insercion")
                lista_f(lista)


            create_person(group_id, save_iden, save_name, picture,  save_p1, save_p2, save_p3) 
            #C:/Users/josue.DESKTOP-PN5D1B3/OneDrive/Escritorio/python/123/josue.jpg


    
        my_window = tk.Tk()
        #GRUPO
        label_1=tk.Label(my_window,text = "Digite su identificación ")
        entry_1 = tk.Entry(my_window) #guarda valor de label_1
        #nOMBRE
        label_3 = tk.Label(my_window, text = "Digite nombre")
        entry_2 = tk.Entry(my_window) #guarda numero
        #URL
        label_5 = tk.Label(my_window, text = "Ingrese el url de la foto ")
        entry_3 = tk.Entry(my_window)
        #P1
        label_6= tk.Label(my_window,text = "Digite la sede donde estudia ")
        entry_4=tk.Entry(my_window)
        #P2
        label_7= tk.Label(my_window,text = "Digite zona de residencia ")
        entry_5=tk.Entry(my_window)
        #P3
        label_8= tk.Label(my_window,text = "Digite carrera a cursar ")
        entry_6=tk.Entry(my_window)
        #Group
        label_9= tk.Label(my_window,text = "Digite el numero de grupo ")
        entry_7=tk.Entry(my_window)

        #BOTON
        button_1 = tk.Button(my_window,text = "Guardar", command=crear_persona)
        
        
        label_2=tk.Label(my_window)
        label_4=tk.Label(my_window)
        label_url=tk.Label(my_window)
        label_p1=tk.Label(my_window)
        label_p2=tk.Label(my_window)
        label_p3=tk.Label(my_window)
        label_group=tk.Label(my_window)
        

        #Solo impresion
        label_1.grid(row=0,column=0)
        label_3.grid(row=1,column= 0)
        label_5.grid(row=2,column=0)
        label_6.grid(row=3,column=0)
        label_7.grid(row=4,column=0)
        label_8.grid(row=5,column=0)
        label_9.grid(row=6,column=0)

        #tomar datos
        entry_1.grid(row=0,column=6)
        entry_2.grid(row=1,column=6)
        entry_3.grid(row=2,column=6)
        entry_4.grid(row=3,column=6)
        entry_5.grid(row=4,column=6)
        entry_6.grid(row=5,column=6)
        entry_7.grid(row=6,column=6)


        button_1.grid(row=7,column=0)
    
    
    def g_ventana3 (self):
        
        def imprimir_persona():
            picture = entry_1.get()
            label_2["text"] = picture

            group_id=entry_2.get()
            label_4["text"] = group_id

            
            def recognize_person(picture, group_id):
    #Detectar si esta foto pertenece a alguien de la familia

                print(picture, " ", group_id)
                response = CF.face.detect(picture)
                face_ids = [d['faceId'] for d in response]
                print(response)    
                identified_faces = CF.face.identify(face_ids, group_id)
                personas = identified_faces[0]
                print(personas)
                candidates_list = personas['candidates']
                candidates = candidates_list[0]
                #print(candidates)
                person = candidates['personId']
                #print(persona)
                person_data = CF.person.get(group_id, person)
                #print(persona_info)
                person_name = person_data['name']
                #print(person_name)
                
                #Detectar si esta foto pertenece a alguien de la familia
                response = CF.face.detect(picture)
                #print(response)
                dic = response[0]
                #print(dic)
                faceRectangle = dic['faceRectangle']
                #print(faceRectangle)
                width = faceRectangle['width']
                top = faceRectangle['top']
                height = faceRectangle['height']
                left = faceRectangle['left']
                image=Image.open(picture)
                draw = ImageDraw.Draw(image)
                draw.rectangle((left,top,left + width,top+height), outline='red')
                font = ImageFont.truetype('C:/Users/josue.DESKTOP-PN5D1B3/Downloads/public_semana-9_Arial_Unicode.ttf', 50)
                draw.text((50, 50), person_name, font=font,  fill="white")
                image.show()
            recognize_person(picture,group_id)
            
            
        
        my_window = tk.Tk()
        #GRUPO
        label_1=tk.Label(my_window,text = "Digite ruta de la foto")
        entry_1 = tk.Entry(my_window) #guarda valor de label_1
        #nOMBRE
        label_3 = tk.Label(my_window, text = "Digite numero de grupo")
        entry_2 = tk.Entry(my_window) #guarda numero
       

        #BOTON
        button_1 = tk.Button(my_window,text = "Guardar", command=imprimir_persona)
        label_2=tk.Label(my_window)
        label_4=tk.Label(my_window)

        #Solo impresion
        label_1.grid(row=0,column=0)
        label_3.grid(row=1,column= 0)


        #tomar datos
        entry_1.grid(row=0,column=6)
        entry_2.grid(row=1,column=6)

        button_1.grid(row=6,column=0)
        #
        

        
  
    
if __name__==__name__:
    root = tk.Tk()
    root.geometry("500x300")

    main=Ventana(root)
    main.pack()
    
root.mainloop()
